# docker-sfdx-cli
Base image to conect to Salesforce DevHub with Salesforce DX CLI

## Sample docker commands
    docker build --rm -t sottama/docker-sfdx-cli:local .
    docker run --rm -it sottama/docker-sfdx-cli:local bin/bash
